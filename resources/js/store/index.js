import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let store = new Vuex.Store({
    state: {
        user: [],
    },

    getters: {
        getAvatar: state => state.user.avatar,
        getUser: state => state.user
    },

    mutations: {
        loadUser(state, payload) {
            state.user = payload;
        }
    },

    actions: {
        loadUser(context, user) {
            axios
                .get('api/users/me')
                .then(response => {
                        user = response.data
                        context.commit('loadUser', user)
                    })
        }
    },
})

export default store;
