require('./bootstrap');

window.Vue = require('vue');

Vue.component('edit', require('./components/tasks/Edit').default);
Vue.component('create', require('./components/tasks/Create').default);
Vue.component('show', require('./components/tasks/Show').default);
Vue.component('index', require('./components/tasks/Index').default);

Vue.component('profile', require('./components/users/Profile').default);

Vue.component('avatar', require('./components/users/Avatar').default);

import Vue from 'vue'
import store from './store/index'

new Vue({
    store,
    el: '#app',
});



