@extends('layouts.base')

@section('title', 'Task list')

@section('content')
   <section class="guest-main">
        <div class="guest-main_image d-flex justify-content-center align-items-center">
            <div class="zoomInDown">
                <div class="d-flex justify-content-center">
                    <a href="{{ route('login') }}"> <img src="{{asset('images/catLogo.png')}}" alt="cat logo" style="width:150px; height:150px;"></a>
                </div>
                <div class="guest-main_hello">Welcome to my app</div>
            </div>
        </div>
   </section>
@endsection
