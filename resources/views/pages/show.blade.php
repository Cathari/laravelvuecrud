@extends('layouts.base')

@section('title', 'Task '.$task->title)

@section('content')
    <div class="container">
        <show v-bind:id ="{{ $task->id }}"></show>
    </div>
@endsection
