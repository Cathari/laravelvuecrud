@extends('layouts.base')

@section('title', 'Edit task '.$task->title)

@section('content')
    <div class="container">
        <edit v-bind:id ="{{ $task->id }}"></edit>
    </div>
@endsection
