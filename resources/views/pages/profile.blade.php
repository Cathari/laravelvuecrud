@extends('layouts.base')

@section('content')
    <div class="container">
        <profile v-bind:id ="{{ $user->id }}"></profile>
    </div>
@endsection
