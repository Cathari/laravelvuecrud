<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserController extends Controller
{

    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function me() {
        return Auth::user();
    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:25|unique:users,name,'.$user->id,
            'email' => 'required|email|max:50|unique:users,email,'.$user->id
            ]);
        $user->update($request->all());
        $user->save();
        return new UserResource($user);
    }

    public function uploadAvatar(ImageRequest $request) {
        if($request->hasFile('image')) {
            $image_path = 'storage/avatar/';
            $image_name = $request->image->getClientOriginalName();
            $request->image->storeAs('public/avatar', $image_name);
            $request->user()->update(['avatar' => $image_path.$image_name]);
        }
        return response(null,200);
    }
}
