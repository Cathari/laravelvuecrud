<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Task as TaskResource;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreTask;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Auth::user()->tasks()->orderBy('order')->get();
        return TaskResource::collection($tasks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return TaskResource
     */
    public function store(StoreTask $request)
    {
        $task = Task::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'user_id' => Auth::id()
        ]);
        $task->save();
        return new TaskResource($task);
    }

    public function show(Task $task)
    {
        return new TaskResource($task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Task $task
     * @return TaskResource
     */
    public function update(StoreTask $request, Task $task)
    {
        $task->update($request->all());
        $task->save();

        return new TaskResource($task);
    }

    public function updateList(Request $request)
    {
        $tasks = Task::all();

        foreach ($tasks as $task) {
            $id = $task->id;
            foreach ($request->tasks as $tasksProp) {
                if ($tasksProp['id'] == $id) {
                    $task->update(['order' => $tasksProp['order']]);
                }
            }
        }

        return response('Updated Successfully.', 200);
    }

    public function uploadFile(Request $request, $id)
    {
        if($request -> hasFile('file')) {
            $task = Task::find($id);
            $task ->addMedia($request->file('file'))->toMediaCollection();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */

    public function destroy($id)
    {
        Task::find($id)->delete();
    }
}
