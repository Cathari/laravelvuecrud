<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:25|unique:users,name,'.$user->id,
            'email' => 'required|email|max:50|unique:users,email,'.$user->id
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Wtf you are doing bro? can you write here your name or not?',
            'name.unique' => 'This name already exists',
            'email.required' => 'Please...stop...write your email.',
            'email.unique' => 'This email already exists'
        ];
    }
}
