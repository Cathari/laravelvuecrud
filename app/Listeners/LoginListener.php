<?php

namespace App\Listeners;

use App\Events\LoginEvent;
use App\Mail\UserLoginEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use Request;

class LoginListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoginEvent  $event
     * @return void
     */

    public function handle(LoginEvent $event)
    {
        $user_ip = Request::ip();
        $arr_ip = geoip()->getLocation($user_ip);
        $user_location = $arr_ip['country'];

        $event->user->update([
            'login_ip' => $user_ip,
            'location' => $user_location
        ]);

        Mail::to($event->user)->send(new UserLoginEmail($event->user));
    }
}
