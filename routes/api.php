<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::put('tasks/update-list', 'API\TaskController@updateList');
Route::post('tasks/upload-file/{task}', 'API\TaskController@uploadFile');

Route::get('tasks/{task}/media', 'API\TaskController@mediaImages');

Route::post('profile/upload-avatar', 'API\UserController@uploadAvatar');

Route::get('users/me', 'API\UserController@me');

Route::apiResource('tasks', 'API\TaskController');
Route::apiResource('users', 'API\UserController');




