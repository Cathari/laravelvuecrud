<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('pages.guest');
});

Route::middleware('auth')->group(function() {
    Route::resource('tasks', 'TaskController');
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::get('profile', 'UserController@profile')->name('profile');

Auth::routes();




